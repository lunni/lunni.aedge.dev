# Getting started with Lunni

!!! example "Public beta"

    **Lunni is in public beta.** It is fairly stable, but there can be bugs.

Lunni is based on Docker Swarm. To install it, you have to get Docker Engine and
enable Swarm mode. You will also need a domain name (subdomain will do). This guide
will walk you through setting Lunni up on a clean server.


## Install Docker Engine

<!-- This section is based on https://github.com/docker/docs, licensed Apache License 2.0. -->
<div class="md-has-sidebar" markdown>
<main markdown>

If you're installing on a clean server, you'll have to install Docker first.
Here's instructions that work for Debian / Ubuntu. If you already have Docker,
[skip to next step](#enable-swarm-mode).

</main>
<aside markdown>

If you're using another distribution, there's a [helpful guide][docker-install] on the
Docker Docs.

</aside>
</div>


[docker-install]: https://docs.docker.com/engine/install/

1.  Update the `apt` package index and install packages to allow `apt` to use a
    repository over HTTPS:

    ```bash
    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg lsb-release
    ```

2. Add Docker's official GPG key:

    ```bash
    # 'debian' or 'ubuntu'
    DISTRO="$(lsb_release -is | tr [:upper:] [:lower:])"
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL "https://download.docker.com/linux/${DISTRO}/gpg" \
      | sudo gpg --dearmor -o '/etc/apt/keyrings/docker.gpg'
    ```

3. Use the following command to set up the repository:

    ```bash
    echo "deb [arch=$(dpkg --print-architecture)" \
      'signed-by=/etc/apt/keyrings/docker.gpg]' \
      "https://download.docker.com/linux/${DISTRO}" \
      "$(lsb_release -cs) stable" \
      | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ```

4. Update the `apt` package index again:

    ```bash
    sudo apt-get update
    ```

5. Install Docker Engine, containerd, and Docker Compose.

    ```bash
    sudo apt-get install \
      docker-ce docker-ce-cli containerd.io docker-compose-plugin
    ```

6. Verify that the Docker Engine installation is successful by running the
   `hello-world` image:

    ```bash
    sudo docker run hello-world
    ```

<!-- End of docker/docs copypasta -->


## Enable Swarm mode

<!-- This section is loosely based on https://dockerswarm.rocks/, which is not freely licensed unfortunately -->

Docker Swarm mode is Docker's built-in orchestrator. It deploys Docker stacks
to production, in a distributed cluster. And it uses the same `docker-compose.yml`
files you're likely using locally for development!

To set it up, run:

```bash
docker swarm init
```

!!! note
    
    If you see an error like this:

    ```
    Error response from daemon: could not choose an IP address to advertise since this system has
    multiple addresses on interface eth0 (138.68.58.48 and 10.19.0.5) - specify one with
    --advertise-addr
    ```

    ...select the public IP (e.g. `138.68.58.48` in this example), and run
    the command again with `--advertise-addr`:

    ```bash
    docker swarm init --advertise-addr 138.68.58.48
    ```

Check that it worked:

```bash
docker node ls
```

!!! note "Multiple nodes"

    You can [add more nodes][3] to the cluster, but it isn't a supported setup with
    Lunni right now. Lunni itself shouldn't break, but the application stacks you
    deploy need to be carefully designed to work with multiple nodes, especially if
    you use volumes.

    In practice, starting with a single node will be easier, and when the time to
    grow comes, you can add more servers and figure out what changes are needed.

[3]: https://dockerswarm.rocks/#add-manager-nodes-optional


## Point a domain to your server

<div class="md-has-sidebar" markdown>
<main markdown>

Lunni expects a wildcard record pointing to your server. This is needed to
allow you to securely access the dashboard – we automatically provision an SSL
certificate from Let's Encrypt. Lunni itself will live on `lunni.[your-domain]`,
and you'll be able to use the rest to deploy your apps.

</main>
<aside markdown>

Don't have a domain? You can get one for free at [Freenom][], or buy it really
cheap at [NameSilo][].

</aside>
</div>

[Freenom]: https://freenom.com/
[NameSilo]: https://www.namesilo.com/register.php?rid=5dae684sv "This is a partner link."

Your domain can be second-level (`acme-apps.cloud`) or a subdomain
(`apps.acme.com`) – Lunni doesn't really care about that.

Add the following records:

- Type: `A`, from: `[your-domain]`, to: your server's IP
- Type: `CNAME`, from: `*.[your-domain]`, to: `[your-domain]`

You can also add an `AAAA` record to support IPv6, but make sure it works
properly first.


## Install Lunni

1. Instal `openssl`:

    ```bash
    sudo apt install openssl
    ```

2. Grab the installer script:

    ```bash
    wget https://aed.ge/setup-lunni.sh
    less setup-lunni.sh
    ```

    Make sure you understand what it does (basically it asks a few questions to
    create a config, downloads [the stack][2] and deploys it).

    [2]: https://gitlab.com/lunni/installer/-/raw/master/stack.yml

3. Run it:

    ```bash
    bash setup-lunni.sh
    ```

4. Fill in your domain name, email, and username and password for Traefik
   dashboard, then confirm setup.

After installation is finished, you'll see a link that will open the following
page:

![Welcome to Lunni](/images/screenshot-welcome.png)

Come up with a username and password for your first admin user. It will have
access to every project deployed on the server, so make sure the password is
good. You can add more users later on.

!!! note

    If you don't create your first admin user, Lunni will shut down after a few
    minutes to prevent others from accessing your server. To restart it, run:

    ```bash
    docker service scale lunni_portainer={0,1}
    ```


## Deploy something

That's it! You now have a Docker Swarm cluster, and a nice dashboard to manage
it. Check out the next sections to find out what you can deploy there (spoiler:
pretty much anything that comes with a `docker-compose.yml` file).
