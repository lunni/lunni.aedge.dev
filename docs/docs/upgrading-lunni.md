From time to time we release updates for Lunni. As auto-update is not yet built
into Lunni itself, you might want to upgrade it manually.

1. As your admin user, in the upper-right corner of any page, click your
   profile photo, then click Settings.

2. In the left sidebar select Lunni updates. Under the `frontend` service,
   click **⋯** → **Update image**.
