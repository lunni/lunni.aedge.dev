# Lunni Marketplace

Lunni Marketplace allows you to deploy some popular web apps with just a few
clicks. You can use one server for both your own applications and third-party
ones. (Just remember: don't put all your eggs in one basket :-)

Here's how you can use Lunni Marketplace:

<div class="md-has-sidebar" markdown>
<main markdown>
1. Open the **:tabler-building-store: Marketplace** section in your Lunni
   dashboard and select one of the available apps.

2. Fill out the configuration parameters and click **Install**.

    - Some parameters (like secret keys) will have a **:tabler-dice-5: Random**
      button. Click it to generate a random value in-browser using the [Web
      Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API).

3. You'll be taken to the freshly created project. Wait for the services to
   deploy, then click **:tabler-arrow-up-right: Open** to go to your freshly
   installed app!
</main>

<aside markdown>

Marketplace is available since our April update. [Make sure you're on the
latest version.](./upgrading-lunni.md)

</aside>
</div>

Currently, we offer a limited selection of apps that you can install, and we
plan to add more as Lunni matures. We welcome suggestions for additional apps
to add to our Marketplace, and you can even contribute app recipes to our repo:
<https://gitlab.com/lunni/marketplace>


??? note "Currently available applications"

    - **Ghost** is a modern, open source platform for companies who are serious
      about content marketing, with lead-gen and newsletters built right in.

    - **PocketBase** is an open source backend consisting of embedded database
      (SQLite) with realtime subscriptions, built-in auth management, convenient
      dashboard UI and simple REST-ish API.

    - **Vaultwarden**: Effortlessly manage all your passwords and logins with
      Vaultwarden, a Bitwarden compatible server perfect for self-hosted deployment.

    - **Forgejo** is a self-hosted lightweight software forge. Easy to install and
      low maintenance, it just does the job. (Gitea fork)

    - **Plausible** is lightweight and open source web analytics. No cookies and
      fully compliant with GDPR, CCPA and PECR.

    - The **Archive Team Warrior**  is a virtual archiving appliance. You can run
      it to help with the Archive Team archiving efforts.

    - **Metabase**: Fast analytics with the friendly UX and integrated tooling to
      let your company explore data on their own.

    - **Chatwoot** gives you tools to manage conversations, build relationships and
      delight your customers from one place.

    - **Lago** is an open-source software for metering and usage-based billing.
      It's an alternative to Chargebee, Recurly or Stripe Billing.
