# Deploy from Docker Hub

Lunni is easy to use if you have even just a little experience with Docker and Docker Compose. You can easily use the same `docker-compose.yml` for development on your local machine and deployment with Lunni.

To try it out, let's deploy Gitea! Gitea provides the following `docker-compose.yml`, which is an excellent starting point:

<div class="md-has-sidebar" markdown>
<main markdown>

```yaml
version: "3"

networks:
  gitea: { external: false }

services:
  server:
    image: gitea/gitea:1.17.3
    container_name: gitea
    environment:
      - USER_UID=1000
      - USER_GID=1000
      - GITEA__database__DB_TYPE=postgres
      - GITEA__database__HOST=db:5432
      - GITEA__database__NAME=gitea
      - GITEA__database__USER=gitea
      - GITEA__database__PASSWD=gitea
    restart: always
    networks: [gitea]
    volumes:
      - ./gitea:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "3000:3000"
      - "222:22"
    depends_on: [db]

  db:
    image: postgres:14
    restart: always
    environment:
      - POSTGRES_USER=gitea
      - POSTGRES_PASSWORD=gitea
      - POSTGRES_DB=gitea
    networks: [gitea]
    volumes:
      - ./postgres:/var/lib/postgresql/data
```
</main>
<aside markdown>

From Gitea Docs: [Installation with Docker and PostgreSQL](https://docs.gitea.io/en-us/install-with-docker/#postgresql-database)

You can also try this with any other `docker-compose.yml` file that uses pre-built images (i. e. all services should have `image` pointing to something that you can pull from Docker Hub or another registry).

</aside>
</div>

## Step 1. Use named volumes

Open up New project page on Lunni, pick a name and paste the Docker Compose file. We'll need to make a few tweaks, but that will only take a minute.

First of all, when deploying with Lunni, you can't mount paths from current directory, as the actual `docker-compose.yml` file is not stored anywhere in the filesystem. Let's use named volumes instead:

<style>
  .highlight .gd + .gi {
    margin-inline-start: calc(.5ch - .125em);
  }
</style>

<pre class="highlight"><code>version: "3"

<span class="gi">volumes:</span>
<span class="gi">  gitea-data:</span>
<span class="gi">  postgres-data:</span>
<span class="c">...</span>
services:
  server:
    <span class="c">...</span>
    volumes:
      - <del class="gd">./gitea</del><span class="gi">gitea-data</span>:/data
<span class="c">... same for postgres</span>
</code></pre>


## Step 2. Define public network and add labels to the HTTP service

Lunni uses Traefik for proxying HTTP. This allows you to run multiple HTTP services on one server, but you'll need to let Traefik know about your service somehow. To do that, add an external network named `traefik-public`:

<pre class="highlight"><code>version: "3"

<span class="c">...</span>
networks:
  gitea: { external: false }
  <span class="gi">traefik-public: { external: true }</span>

services:
  server:
    <span class="c">...</span>
    networks: [gitea<span class="gi">, traefik-public</span>]
<span class="c">...</span>
</code></pre>

Remove the HTTP port (which will go through Traefik instead):

<pre class="highlight"><code>services:
  server:
    <span class="c">...</span>
    ports:
      <del class="gd">- "3000:3000"</del>
      - "222:22"
<span class="c">...</span>
</code></pre>

Then add the labels for Traefik to pick up:

<pre class="highlight"><code>services:
  server:
    <span class="c">...</span>
    <span class="gi">deploy:</span>
    <span class="gi">  labels:</span>
    <span class="gi">    - traefik.enable=true</span>
    <span class="gi">    - traefik.docker.network=traefik-public</span>
    <span class="gi">    - traefik.constraint-label=traefik-public</span>
    <span class="gi">    - traefik.http.routers.${PROJECT_NAME?}-http.rule=Host(`${DOMAIN?}`)</span>
    <span class="gi">    - traefik.http.routers.${PROJECT_NAME?}-http.entrypoints=http</span>
    <span class="gi">    - traefik.http.routers.${PROJECT_NAME?}-http.middlewares=https-redirect</span>
    <span class="gi">    - traefik.http.routers.${PROJECT_NAME?}-https.rule=Host(`${DOMAIN?}`)</span>
    <span class="gi">    - traefik.http.routers.${PROJECT_NAME?}-https.entrypoints=https</span>
    <span class="gi">    - traefik.http.routers.${PROJECT_NAME?}-https.tls=true</span>
    <span class="gi">    - traefik.http.routers.${PROJECT_NAME?}-https.tls.certresolver=le</span>
    <span class="gi">    - traefik.http.services.${PROJECT_NAME?}.loadbalancer.server.port=3000</span>
<span class="c">...</span>
</code></pre>

This is quite lengthy, but if you're using editor on the New project page, you can just start typing “deploy” and hit <kbd>Enter</kbd> to get this whole piece autocompleted. You'll just need to fill out the port and possibly tweak the routing rules.

Let's also define `GITEA__server__ROOT_URL` so that Gitea knows where it lives:

<pre class="highlight"><code>services:
  server:
    <span class="c">...</span>
    environment:
      - USER_UID=1000
      - USER_GID=1000
      - GITEA__database__DB_TYPE=postgres
      - GITEA__database__HOST=db:5432
      - GITEA__database__NAME=gitea
      - GITEA__database__USER=gitea
      - GITEA__database__PASSWD=gitea
      <span class="gi">- GITEA__server__ROOT_URL=https://${DOMAIN?}/</span>
<span class="c">...</span>
</code></pre>


## Step 3. Define the variables

Right below the Compose file editor, you'll find another one for Environment variables. Those will be substituted in the Compose file we made, and we have two of them:

- `PROJECT_NAME` which will be added by Lunni automatically, and
- `DOMAIN` which we need to define ourselves.

This is fairly straightforward. For example, if your domain will be `gitea.example.net`, just type:

```shell
DOMAIN="gitea.example.net"
```

Make sure that `DOMAIN` actually points to your server (e. g. use a <abbr>CNAME</abbr> record).

!!! tip "`DOMAIN` variable"
    `DOMAIN` is a special variable in Lunni. If it is defined on a project, Lunni will show a link to `https://${DOMAIN}` on the project page, so that you can quickly access the HTTP service.


## Step 4. Hit deploy

This is it! Let's go through our changed Compose file once again:

<!-- sorry not sorry -->
<div class="md-has-sidebar code-cont" markdown>
<main markdown>

```yaml
version: "3"

volumes:
  gitea-data:
  postgres-data:

```
</main>
<aside>1. Use named volumes instead of paths.</aside>
</div>


<div class="md-has-sidebar code-cont" markdown>
<main markdown>

```yaml
networks:
  gitea: { external: false }
  traefik-public: { external: true }

services:
  server:
    image: gitea/gitea:1.17.3
    container_name: gitea
    environment:
      - USER_UID=1000
      - USER_GID=1000
      - GITEA__database__DB_TYPE=postgres
      - GITEA__database__HOST=db:5432
      - GITEA__database__NAME=gitea
      - GITEA__database__USER=gitea
      - GITEA__database__PASSWD=gitea
      - GITEA__server__ROOT_URL=https://${DOMAIN?}/
    restart: always
    networks: [gitea, traefik-public]
    volumes:
      - gitea-data:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
```
</main>
<aside>2. Define external <code>traefik-public</code> network. Use it on public-facing services.</aside>
</div>

<div class="md-has-sidebar code-cont" markdown>
<main markdown>

```yaml
    ports: 
      # - "3000:3000"
      - "222:22"
    deploy:
      labels:
        - traefik.enable=true
        - traefik.docker.network=traefik-public
        - traefik.constraint-label=traefik-public
        - traefik.http.routers.${PROJECT_NAME?}-http.rule=Host(`${DOMAIN?}`)
        - traefik.http.routers.${PROJECT_NAME?}-http.entrypoints=http
        - traefik.http.routers.${PROJECT_NAME?}-http.middlewares=https-redirect
        - traefik.http.routers.${PROJECT_NAME?}-https.rule=Host(`${DOMAIN?}`)
        - traefik.http.routers.${PROJECT_NAME?}-https.entrypoints=https
        - traefik.http.routers.${PROJECT_NAME?}-https.tls=true
        - traefik.http.routers.${PROJECT_NAME?}-https.tls.certresolver=le
        - traefik.http.services.${PROJECT_NAME?}.loadbalancer.server.port=3000
    depends_on: [db]
```

</main>
<aside>3. Don't expose HTTP port directly. Instead, add labels for Traefik to find it and proxy.</aside>
</div>

<div class="md-has-sidebar code-cont" markdown>
<main markdown>

```yaml
  db:
    image: postgres:14
    restart: always
    environment:
      - POSTGRES_USER=gitea
      - POSTGRES_PASSWORD=gitea
      - POSTGRES_DB=gitea
    networks: [gitea]
    volumes:
      - postgres-data:/var/lib/postgresql/data
```

</main>
<aside><code>db</code> is not exposed publicly, so don't add any networks or labels (but still use a named volume)</aside>
</div>

If everything looks alright, click Create project and you're good to go!

!!! note "Unsupported options"
    Docker Swarm ignores some options, like `restart` or `depends_on`. You can safely leave those in (e. g. if you use your compose file for both local development and deployment). See the [Compose file v3 reference][cv3] for more information.

    Lunni will warn you about these in a later version.

[cv3]: https://docs.docker.com/compose/compose-file/compose-file-v3/
