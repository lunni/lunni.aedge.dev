# Pricing: $0

Lunni is completely free! It is easy and fun to self host.

However, if you don't want to do it yourself, we can run Lunni for you for a
small monthly fee. We're currently evaluating the best way to provide such a
service, and if that's something you'd be interested in, [drop us a line and
let's chat][mailto]!

Alternatively, if you just want to support us (thank you! &lt;3), we have an
[Open Collective][] page, and our lead developer has [GitHub Sponsors][]
profile.

[mailto]: mailto:hello@aedge.dev?subject=Lunni%20Cloud
[Open Collective]: https://opencollective.com/lunni
[GitHub Sponsors]: https://github.com/sponsors/notpushkin
